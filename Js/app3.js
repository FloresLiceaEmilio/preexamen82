const img = document.getElementById('img');
const select = document.getElementById('razas');
const btn1 = document.getElementById('CargarRazas');
const btn2 = document.getElementById('CargarImagen');

btn1.addEventListener('click', function(){
    try{
        mostrarrazas('https://dog.ceo/api/breeds/list');
    } catch(error){
        const p = document.createElement('p')

        p.innerText = 'No se encotraron datos';
        error.appendChild(p)

    }
   
});

btn2.addEventListener('click', function(){
    try{
        mostrarIMG(select.value);
    } catch(error){
        const p = document.createElement('p')

        p.innerText = 'No se encotraron datos';
        error.appendChild(p)

    }
   
});


function mostrarrazas(url){
    limpiar();
    axios.get(url)
    .then(response => {
        const razas = response.data;
        razas.message.forEach(raza => {
            const op = document.createElement('option')
            op.value = raza;
            op.innerText = raza;
            select.appendChild(op);
        });
    })
    
}

function mostrarIMG(raza = 'affenpinscher'){
    limpiarIMG();
    const url = `https://dog.ceo/api/breed/${raza}/images/random`;
    axios.get(url)
        .then(response => {
            const imag = response.data;
            const imagen = document.createElement('img');
            imagen.src = imag.message;
            img.appendChild(imagen);
        })
        .catch(error => {
            const p = document.createElement('p');
            p.innerText = 'No se encontraron datos';
            img.appendChild(p);
        });
}

function limpiar() {
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
}

function limpiarIMG() {
    while (img.firstChild) {
        img.removeChild(img.firstChild);
    }
}
